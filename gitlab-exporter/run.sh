#!/bin/bash

# configure exporter gitlab token
sed -i "s/<GITLAB-TOKEN>/$GITLAB_TOKEN/" /app/config.yaml

# export all project to /app/data
pipenv run gitlab-project-export.py -c config.yaml -d

# upload all projects to S3 bucket
pipenv run aws s3 cp /app/data s3://$AWS_BUCKET_STORE_NAME --recursive
