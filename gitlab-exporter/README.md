# Gitlab exporter


Docker project to export all project from gitlab to AWS S3.

## Build


```bash

docker build . -t gitlab-exporter

```

## Run Container


```bash

docker run -e "AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>" -e "AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>" -e "AWS_BUCKET_STORE_NAME=<BUCKET_STORE_NAME>"  -e "GITLAB_TOKEN=<GITLAB_TOKEN>" gitlab-exporter

```
