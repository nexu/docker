# Docker Images

There is one directory for each Dockerfile. Docker images are built by GitLab when a git tag is created with the following pattern: `directory-version`, i.e. `my-image-1.0.0`. Semantic versioning **must** be used following the last dash.
