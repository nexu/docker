#!/bin/bash
#
# Prepare the files so GitLab builds the image for the given directory.
#
# As GitLab runs the build command in the root directory, this script copies the
# files from a directory to the root (one level above).
#
# The argument must be directory-version, where version must follow the semantic
# versioning pattern. This script will fail if the directory does not exist or
# the version is bad formed.

TAG=$1
if [ -z $TAG ]; then
  echo 'Missing argument of type "directory-semver", i.e: "mydir-1.0.0".'
  exit 1
elif ! echo $TAG | grep -q '^\(\w\+-\)\+[0-9]\.[0-9]\.[0-9]$'; then
  echo 'Invalid tag. Use the pattern "directory-semver, i.e: "mydir-1.0.0".'
  exit 1
fi

DIR=${TAG%-*}
SEMVER=${TAG##*-}

# Validate directory and semantic version
if [ -z $DIR ]; then
  echo 'Missing directory name. Use "directory-semver."'
  exit 1
elif [ ! -f $DIR/Dockerfile ]; then
  echo "ERROR: \"$DIR/Dockerfile\" not found."
  exit 1
elif ! echo $SEMVER | grep -q '^[0-9]\.[0-9]\.[0-9]$'; then
  echo "Version \"$SEMVER\" is not semantic. Follow https://semver.org."
  exit 1
fi

# Copy the directory files to the root folder
cp -fr $DIR/* .
