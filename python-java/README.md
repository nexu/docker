# Container with Java and Python

This container contains java openjdk-11-jre-headless and python 3.9

## How to Build the image

```bash

  docker build -t python-java .
```
