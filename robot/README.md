# Robot Framework

This image contains the Robot Framework. It doesn't have an `ENTRYPOINT` or
`CMD` because its purpose is to be used in GitLab CI.

## GitLab execution

In the `.gitlab-ci.yml` file of your project, add:

```
---
include:
  - project: nexu/docker
    file: /robot/.gitlab-ci-template.yml

robot-tests:
  variables:
    TESTS: my/test/dir
```

If necessary, you can override other settings of the template, i.e. "rules".

## Manual execution
To run robot in your local computer, for example, run (replace `x.y.z` with the
latest version):

```
docker run --rm gitlab.com/nexu/docker:robot-x.y.z robot --help
```
